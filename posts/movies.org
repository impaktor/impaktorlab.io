#+TITLE: Memory dump of Movies
#+DATE: <2024-12-20 Fri>
#+SETUPFILE: ../org-templates/post.org
#+OPTIONS: H:4 tex:t author:nil ^:nil email:nil toc:nil
#+KEYWORDS: movies 80s films recommendations list
#+DESCRIPTION: List of moives I often mention
#+LANGUAGE: en
#+STARTUP: content nolineimages

#+begin_verse
There is nothing real outside our perception of reality
#+end_verse
/Videodrome/

#+TOC: headlines 3

* Introduction
As I'm leaving my current employment, colleagues asked me to make a list of
movies I often reference and/or quote in discussions. The following post is
simply a list of movies I could think of from the top of my head, and makes no
claims to be "definitive" or "complete".

They are split into different categories, because it did not feel right to
have movies like [[https://www.imdb.com/title/tt0094612/][Action Jackson]] or [[https://www.imdb.com/title/tt0102984/][Stone Cold]] with [[https://www.imdb.com/title/tt0338564][Infernal Affairs]].

* "Foreign" / Non-Hollywood
** Hong Kong
  - [[https://www.imdb.com/title/tt0338564][Infernal Affairs]]
  - [[https://www.imdb.com/title/tt0097202/][The Killer]] (Hong Kong/Chow Yun-Fat)
  - [[https://www.imdb.com/title/tt0098691/][A Better Tomorrow]] (Hong Kong/Chow Yun-Fat)
  - [[https://www.imdb.com/title/tt0113326/][Rumble in the Bronx]] (Hong Kong/Jackie Chan)
  - (Police Story 2) (Jackie Chan)
  - [[https://www.imdb.com/title/tt0081506/][Shogun assassin]]
** South Korea
   - Vengeance trilogy (Park Chan-Wook):
     - [[https://www.imdb.com/title/tt0310775/][Sympathy for Mr. Vengeance]]
     - [[https://www.imdb.com/title/tt0364569/][Oldboy]]
     - [[https://www.imdb.com/title/tt0451094/][Lady Vengeance]]

   - [[https://www.imdb.com/title/tt0374546/][Spring, Summer, Fall, Winter... and Spring]]

   Bong Joon Ho:
   - [[https://www.imdb.com/title/tt0762073/][Thirst]] (aka. Bakjwi)
   - [[https://www.imdb.com/title/tt0468492/][The Host]]
   - [[https://www.imdb.com/title/tt6751668/][Parasite]]
   - [[https://www.imdb.com/title/tt0353969/][Memories Of Murder]] ([[https://tv.nrk.no/program/KOIF11003521][NRK]])
** China
   - In the mood for love
   - Hero (aka. Ying xiong)
   - House of Flying Daggers (aka. Shi mian mai fu)
   - Zu Warriors from the Magic Mountain
** Israel
   - [[https://www.imdb.com/title/tt2309224/][Big Bad Wolves]]
** Brazil
   - [[https://www.imdb.com/title/tt0861739/][Tropa de Elite]]
   - [[https://www.imdb.com/title/tt0317248][City of God]]
** Sweden
   - [[https://www.imdb.com/title/tt6725158][Fanny & Alexander]] (preferably the 6 h mini-series version)
** France
   - Taxi 2

   - The Three Colours trilogy (Krzysztof Kieślowski):
     - Three Colours: Blue (1993),
     - Three Colours: White (1994),
     - Three Colours: Red (1994),

   - The Cook, the Thief, His Wife & Her Lover

  - [[https://www.imdb.com/title/tt0070544/][The Fantastic Planet]]
** Germany
   - Das Leben der Anderen (aka. "The Lives of Others")
   - Run lola run
   - Wir sind die nacht
   - Er ist wieder da ("Look who's back")
** Spain
    - Pan's Labyrinth / The Labyrinth of the Faun (El laberinto del fauno)
    - Timecrimes (Los cronocrímenes)
    - Sleep Tight
** Argentine
   - Wild Tales (aka "Relatos salvajes", 2014)
* Movies / American
** John Carpenter
   Mandatory!

   - [[https://www.imdb.com/title/tt0084787/][The Thing]]
   - Big Trouble in Little China
   - They Live
   - Escape from New York
   - Prince of Darkness
   - Assault on Precinct 13
   - Christine (1983)
** John Huges / 80s high school
   Very mandatory!

   - Ferris Buller's day off
   - Breakfast club
   - Sixteen candles
   - Some kind of wonderful
   - Career Opportunities (script)

   - (Pretty in pink)
   - (Weird science)

   Not Hughes but could have been:
   - Say Anything
   - Cant Buy me Love
   - Heathers
   - Mannequin
   - Real Genius
   - Fast times at Ridgemont High
** Arnold
   Super-duper mandatory
   - Terminator 1 & 2
   - Conan 1 & 2
   - Commando
   - Predator
   - True Lies
   - Total Recall
   - The Running Man
   - Raw Deal
   - Red Heat
   - (Pumping Iron)
   - Twins
   - [[https://www.imdb.com/title/tt0099938/][Kindergarten Cop]]
** Not-Arnold High Testosterone-action
   - [[https://www.imdb.com/title/tt0105698/][Universal Soldier]] (1991, Dolph + van Damme)
   - Tango and Cash
   - RoboCop 1-2 (1987, 1990)
   - Die Hard 1-2
   - Rambo 1-3
   - Leathal Weapon 1-2
   - [[https://www.imdb.com/title/tt0102266/][The Last Boy Scout]]
   - Cobra
   - The Long Kiss Goodnight
   - Stone Cold
   - Beyond the Law (C. Sheen)
   - Mad Max 2 (and 3)
   - 48 Hrs & 48 Hours
   - John Wick 1-3 + [[https://www.imdb.com/title/tt7888964/][Nobody]] (2021)
   - xXx (Vin Diesel)
   - [[https://www.imdb.com/title/tt0094612/][Action Jackson]]
   - [[https://www.imdb.com/title/tt0108171/][Sniper]]
   - /Dolph Lundgren/ (in descending order)
     - [[https://www.imdb.com/title/tt0098141/][The Punisher]]
     - [[https://www.imdb.com/title/tt0102915/][Showdown in Little Tokyo]]
     - [[https://www.imdb.com/title/tt0106309/][Joshua Tree]]
     - [[https://www.imdb.com/title/tt0099817/][Dark Angel]]
     - [[https://www.imdb.com/title/tt0110490/][Men of War]]

   - /Jean-Claude van Damme/ (in descending order)
     - Double Impact
     - [[https://www.imdb.com/title/tt0126388/][Legionnaire]]
     - Hard Target
     - Timecop
     - Lionheart
     - Bloodsport
     - Sudden death

   - /Steven Segal/ (in descending order)
     - Under Siege 1 (2)
     - [[https://www.imdb.com/title/tt0094602/][Above the law]] (Steven Segal)
     - Hard to Kill
     - Out for Justice
     - [[https://www.imdb.com/title/tt0119123/][Fire down Below]]

   - /Wesley Snipes/ (in descending order)
     - Demolition Man
     - Blade (W. Snipes)
     - Passenger 57 (W. Snipes)

   - ... /Chuck Norris/

   - Deathstalker I (Conan-clone + scantly clad wamen)
   - Hell Comes to Frogtown ([[https://odysee.com/hell-comes-to-frogtown-(1988):1][full movie]]) (Mad Max-clone with Randy Piper)
** Classic "oldies"
   - [[https://www.imdb.com/title/tt0050083/][12 Angry Men]] (1957)
   - [[https://www.imdb.com/title/tt0057012/][Dr. Strangelove]] (1964)
   - It's A Wonderful Life (1946)
   - Paths of Glory (1957)
   - Some Like It Hot (1959)
   - Spartacus (1960)
   - The Conversation (1974)
   - The Day The Earth Stood Still (1951)
   - The Great Escape (1963)
   - The Killing (1956)

** 70s Classics
   - Dirty Harry (1-3)
   - Network (1976)
   - Taxi Driver
   - [[https://www.imdb.com/title/tt0077416/][The Deer Hunter]]
   - Scarface
   - Bullet
   - The Godfather 1,2,3
   - The French Connection
   - Jaws
   - Close Encounters of the Third Kind
   - Planet of the apes (technically from the 60s)
   - One flew over the cuckoo's nest
   - The Shining
   - Apocalypse Now
   - A Clockwork Orange
   - Smokey and the Bandit
   - Kelly's Heroes
   - Where Eagles Dare
   - The Elephant man
   - Easy Rider
   - Superman
   - Vanishing Point
   - [[https://www.imdb.com/title/tt0068473/][Deliverance]]
** 80s Classics
   - Back to the Future 1-3
   - Indiana Jones 1-3
   - Beverly Hills Cop 1-2
   - [[https://www.imdb.com/title/tt0091203/][Highlander]]
   - [[https://www.imdb.com/title/tt0086567/][WarGames]]
   - Ghostbusters 1 (2)
   - Top Gun
   - [[https://www.imdb.com/title/tt0083658/][Blade Runner]]
   - Coming to America
   - Trading Places
   - Blues Brothers
   - Beetle Juice
   - Big (1988)
   - [[https://www.imdb.com/title/tt0082136/][Cannonball run I]] (1981)
   - Gremlins
   - Ground hog day
   - Iron eagle
   - ET
   - Planes Trains Automobiles
   - The Platoon
   - When Harry Met Sally
   - Wallstreet
   - Princess Bride
   - [[https://www.imdb.com/title/tt0097637/][K-9]]
   - Overboard
   - Rain man
   - Turner and Hootch
   - [[https://www.imdb.com/title/tt0096764/][The Adventures of Baron Munchausen]]
   - The Fly
   - [[https://www.imdb.com/title/tt0093058/][Full metal jacket]]
   - ([[https://www.imdb.com/title/tt0100135/][Men at work]])
   - [[https://www.imdb.com/title/tt0098577][Vampire's Kiss]]
   - [[https://www.imdb.com/title/tt0100935/][Wild at Heart]]
   - Rocky I, II, III, IV, V

   Possibly children's movies but still classic
   - Flight of the navigator
   - Goonies
** 90s (00s) Classics
   - [[https://www.imdb.com/title/tt0113277/][Heat]]
   - 12 Monkeys
   - Falling Down
   - Batman 1-2 (Michael Keaton)
   - Con Air
   - Point Break
   - The Rock
   - Executive Decision
   - Speed
   - Face/Off
   - Silence of the Lambs
   - Blow
   - A few good men
   - American History x
   - Donnie Brasco (based on true story)
   - Carlito's way (Some really funny Pacino-lines in this one)
   - Fearing and Loathing in Las Vegas
   - The Rookie
   - Leon
   - [[https://www.imdb.com/title/tt0113845/][Money Train]]
   - Lock, Stock and Two Smoking Barrels
   - [[https://www.imdb.com/title/tt0113627/][Leaving Las Vegas]]
   - From dusk til dawn
   - Everything by Quentin Tarantino, including "[[https://www.imdb.com/title/tt0110632/][Natural Born Killers]]" & "[[https://www.imdb.com/title/tt0108399/][True Romance]]"
   - True Romance (1993)
   - Jurassic park
   - Men in Black
   - Seven (se7en)
   - Shawshank redemption
   - Sleepers
   - The hunt for Red October
   - Pirates of Silicon Valley
   - The Devil's Advocate (1997)
   - [[https://www.imdb.com/title/tt0117060/][Mission: Impossible]]
   - The Fugitive
   - [[https://www.imdb.com/title/tt0120873/][US Marshals]]
   - The Game
   - The Usual Suspects
   - American Psycho
   - My Cousin Vinny
   - Tremors 1 (2?)
   - L.A. Confidential
   - [[https://www.imdb.com/title/tt0106881/][Fearless]]
   - Scent of a Woman
   - Broken Arrow
   - Nikita
   - Kiss kiss bang bang (2005)
   - White men can't jump
   - Crimson tide
   - Premium Rush
   - Killing Zoe
   - Pulp Fiction
   - Reservoir Dogs
   - [[https://www.imdb.com/title/tt0116695][Jerry Maguire]]
   - The Bad Lieutenant
   - [[https://www.imdb.com/title/tt0113481/][Johnny Mnemonic]]
   - [[https://www.imdb.com/title/tt0144117/][The Boondock Saints]]
   - Judge Dredd
   - [[https://www.imdb.com/title/tt0107969/][Rising Sun]] (S. Connery, W. Snipes)
   - [[https://www.imdb.com/title/tt0116277/][The Fan]] (De Niro, W. Snipes)
   - [[https://www.imdb.com/title/tt0119731/][Murder at 1600]]
   - [[https://www.imdb.com/title/tt0144550/][The Pentagon Wars]]
   - [[https://www.imdb.com/title/tt0186151/][Frequency]]
   - [[https://www.imdb.com/title/tt0117333/][Phenomenon]]
   - [[https://www.imdb.com/title/tt0443496/][Edmond]] (William H. Macy)
   - [[https://www.imdb.com/title/tt2179116/][The Kings of Summer]] (The "Stand by Me" of the 2010s?)
   - The Matrix
** (Pretentious?) Drama
   - Remains of the Day
   - Stand by me
   - Magnolia
   - Vanilla Sky
   - Donnie Darko
   - Station agent
   - Me and you and everyone we know
   - Melancholia
   - The Wrestler (by Aronofsky, others: Black Swan, Pi, The Fountain)
   - American Beuty (Kevin Spacey)
   - [[https://www.imdb.com/title/tt0112471/][Before Sunrise]] / Sunset / Midnight
   - Lost in Translation
   - Karate kid
   - Labyrinth
   - Sideways
   - Ghost World
   - American Splendor
   - Punch-Drunk Love
   - Into the Wild
   - Garden State
   - Eternal Sunshine of the Spotless Mind
   - The Woodsman
   - Another Earth
   - Memento (Christopher Nolan)
   - Carnage (Jodie Foster, Christopher Waltz, John C Riley)
   - Locke (Tom Hardy)
** Wes Anderson
   - [[https://www.imdb.com/title/tt0265666/][The Royal Tenenbaums]]
   - Rushmore
   - Bottle Rocket
   - ...and everything else by him
** Comedy
   - Old School
   - Blazing Saddles (Gene Wilder, Mel Brooks)
   - Young Frankenstein (Gene Wilder, Mel Brooks)
   - Office Space
   - [[https://www.imdb.com/title/tt0120654/][Dirty Work]] (Norm MacDonald)
   - Napoleon Dynamite
   - Life of Brian
   - Monty Python and the Holy Grail
   - A Fish Called Wanda
   - Uncle Buck (John Candy)
   - Wedding Crashes
   - Weekend at Bernie's
   - Stripes (Bill Murray, John Candy)
   - Super
   - Super Bad
   - Anchorman
   - Armed and dangerous (John Candy)
   - Back to school
   - Encino man
   - Step Brothers
   - The Naked Gun
   - Super Troopers
   - Hot Shots (Dependency: Top Gun)
   - Hot Shots 2 (Dependency: Rambo 2-3, Total Recall, Wallstreet, Platoon, Apocalypse Now)
   - Airheads (Brendan Fraser, Adam Sandler and Steve Buccemi hijack a radio station)
   - Blast from the Past (1999)
   - Groundhog day
   - Police Academy (1)
   - Wayne's World 1 (2)
   - Austin Powers 1-3
   - Bill & Ted's Excellent Adventure
   - Bill & Ted's Bogus Journey
   - National Lampoon's Vacation
   - National Lampoon's European Vacation
   - [[https://www.imdb.com/title/tt0116778/][Kingpin]]
   - Team America: World Police
   - [[https://www.imdb.com/title/tt0119822/][As Good as it Gets]]
   - [[https://www.imdb.com/title/tt0305224/][Anger management]]
   - [[https://www.imdb.com/title/tt0286947/][Scorched]] (2003)
   - [[https://www.imdb.com/title/tt2294965/][Zero Charisma]]
   - Tropic Thunder
** Sci-fi
   I'm intentionally excluding Armageddon, Deep Impact, Star Trek.

   - Contact (Jodie Foster)
   - The 5th element
   - Equilibrium (Christian Bale, really cool fighting in this one)
   - Gattaca (Uma Thurman and Ethan Hawke)
   - Primer
   - Cube
   - Event Horizon
   - Ultraviolet
   - Aeon Flux
   - Sphere (1998 with Dustin Hoffman and Samuel L. Jackson)
   - The Abyss (1989 Ed Harris)
   - Stargate (Kurt Russel)
   - Star Wars (4-6)
   - Starship Troopers
   - Repo Man (1984)
   - The Hidden (1987)
   - The Keep (1983)
   - Wedlock (1991)
   - Strange Days (Ralph Finnes)
   - The Thirteenth floor
   - Dark City
   - Pitch Black (Vin Diesel)
   - Alien (1) & Aliens (2)
   - Minority Report

   - [[https://www.imdb.com/title/tt0049223/][The Forbidden Planet]] (1956)
   - [[https://www.imdb.com/title/tt0060397/][The Fantastic Voyage]] (1966)
   - [[https://www.imdb.com/title/tt0087597/][The Last Starfighter]]
   - Soylent Green
   - Yor the hunter from the Future
   - Dreamscape
   - [[https://www.imdb.com/title/tt0089114/][Explorers]]
   - Inner Space
   - Flash Gordon
   - Tron
   - Krull
   - [[https://www.imdb.com/title/tt0088172/][Starman]]
   - The Adventures of Backaroo Banzai
   - [[https://www.imdb.com/title/tt0094631/][Alien Nation]]
   - [[https://www.imdb.com/title/tt0085271/][Brainstorm]]
   - [[https://www.imdb.com/title/tt0070909/][Westworld]] (1973)
   - Logan's run
   - Dark Star
   - Battle Beyond the Stars
   - Zardoz
   - [[https://www.imdb.com/title/tt0067756/][Silent Running]]
   - (Rocky Horror Picture)
   - ([[https://www.imdb.com/title/tt0074851/][The man who fell to earth]])


   Fantasy:
   - [[https://www.imdb.com/title/tt0093779/][The Princess Bride]] (1987)
   - [[https://www.imdb.com/title/tt0088323/][Never Ending Story]] (1984)
   - [[https://www.imdb.com/title/tt0082186/][Clash of the titans]] (1981)
   - [[https://www.imdb.com/title/tt0096446/][Willow]] (1988)
   - [[https://www.imdb.com/title/tt0083791/][The Dark Crystal]] (1982)
   - Gremlings (1 & 2)
   - Dragonheart

** Joel & Ethan Choen
   - Big Lebowski
   - Fargo
   - Blood Simple (Joel & Ethan Choen, 1984)
   - Burn after reading
   - ...and much more
** David Lynch
   - Blue Velvet
   - Twin Peaks (series)
   - Lost Highway
   - Mullholland Drive
   - (Dune)
** Kevin Smith
   Classic 90s films.
   - Clerks
   - Mallrats
   - Chasing Amy
   - Dogma
   - Amy
   - Clerks 2
** Clint Eastwood
   - Dirty Harry 1-3
   - Unforgiven
   - Pale Rider
   - Heartbreak Ridge
   - Any which way but loose (1978)
   - Any which way you can (1980)
** "So bad it's good"-movies
   - [[https://www.imdb.com/title/tt0130236/][Samurai Cop]] (Some scenes were recorded after the main character got a
     haircut, thus the lady wig)
   - [[https://www.imdb.com/title/tt0368226/][The Room]] (so classic it spawned [[https://www.imdb.com/title/tt3521126/][The Disaster Artist]])
* Epilogue
  Note to self: Why isn't the 80s list a mile long? Looks suspiciously short.
  Missing plenty of other classics, like [[https://www.imdb.com/title/tt0090142/][Teen Wolf]]?

  There are plenty more movies I should add, but don't have the energy to, e.g.

  Actors:
  - Much more Gene Hackman!
  - Most Adam Sandler - yes!
  - [[https://www.imdb.com/name/nm0005377][Sam Rockwell]]

  Directors:
  - Most [[https://www.imdb.com/name/nm0000343/][David Cronenberg]]
  - Most [[https://www.imdb.com/name/nm0000040/][Stanley Kubrick]]
  - Most Quentin Tarantino

  Genres:
  - Much James Bond
  - Some "Gangster" movies
  - Star Wars / Star Trek
